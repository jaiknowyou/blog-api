const mongoose = require("mongoose");
const Author = require("./author");

//var ObjectId = require("mongodb").ObjectID;

var ObjectId = mongoose.Schema.ObjectID;

const blogschema = mongoose.Schema({
  //blogId: Number,
  title: String,
  Description: String,
  EstimatedReadMinutes: Number,
  //AuthorId: {
    //type: mongoose.Schema.Types.ObjectID,
    //ref: Author,
  //},
});

const blogs = mongoose.model("blog", blogschema);

module.exports = blogs;
