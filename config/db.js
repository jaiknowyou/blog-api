const mongoose = require("mongoose");

const config = require("config");

//const uri=config.get("DBuri");

const ConnectDB = async () => {
  mongoose.connect(
    "mongodb+srv://user:user@cluster0.fruun.mongodb.net/userdata?retryWrites=true&w=majority",
    { useUnifiedTopology: true, useNewUrlParser: true }
  );
  console.log("Database userdata Connected");
};

module.exports=ConnectDB;
