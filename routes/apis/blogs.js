const express = require("express");
const blogs = require("../../models/Blog");

const router = express.Router();

router.post("/post", async (req, res) => {
  const { title, des,time } = req.body;

  const blog = new blogs({
    title,
    des,
    time,
  });
  await blog.save();
  res.json(blog);
  console.log("Blog posted");
});

router.put("/modifyblog/:id", async (req, res) => {
  try {
    const id = req.params.id;
    let blog = await blogs.findById(id);
    blog.Description = req.body.Description;
    await blog.save();
    res.status(200).json(blog);
  } catch (err) {
    console.error(err.message);
    res.status(400).json({ error: err });
  }
});

router.get("/getblog/:id", async (req, res) => {
  try {
    const id = req.params.id;
    let blog = await blogs.findById(id);
    res.status(200).json(blog);
  } catch (err) {
    console.error(err.message);
    res.status(400).json({ error: err });
  }
});

router.get("/getallblogs", async (req, res) => {
  try {
    res.status(200).json(blogs);
  } catch (err) {
    console.error(err.message);
    res.status(400).json({ error: err });
  }
});

router.delete("/deleteUser/:id", async (req, res) => {
  try {
    const id = req.params.id;
    await blogs.findByIdAndDelete(id); // delete a single element
    res.status(200).json("deleted");
  } catch (err) {
    console.error(err.message);
    res.status(400).json({ error: err });
  }
});



module.exports = router;
