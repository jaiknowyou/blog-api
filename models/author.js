const mongoose = require("mongoose");

const authorSchema = mongoose.Schema({
  name: String,
  TotalBlogs: Number,
});

const Author = mongoose.model("authors", authorSchema);

module.exports = Author;
