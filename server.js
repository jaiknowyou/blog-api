const express = require("express");
const connectDB = require("./config/db");

const app=express();
app.use(express.json());

connectDB();

app.get("/",(req,res)=>res.send("Main Page"));

app.use("/blogs",require("./routes/apis/blogs"));

app.use("/authors",require("./routes/apis/author"));

app.listen(5000, () => console.log("Server running at 5000"));