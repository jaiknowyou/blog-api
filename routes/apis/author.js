const express = require("express");
const { isValidObjectId } = require("mongoose");
const Author = require("../../models/author");

const router = express.Router();

router.post("/post", async (req, res) => {
  const { name, TotalBlogs } = req.body;
  const author = new Author({name, TotalBlogs});
  await author.save();
  res.status(200).json(author);
  console.log("Details Saved Successfully");
});



module.exports = router;
